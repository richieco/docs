After completing the installation and the Quick Start below, you will be able to deploy any function you'd like to the a8 platform. Please let us know of an issues by opening an issue in the appropriate repository. [Join our slack group](https://a8-slack.herokuapp.com) if you have any questions or want to get involved with our developer community.

# Installing a8

## Pre-requisites

1. [Install docker](https://docs.docker.com/v17.12/install/)
2. Create an account on [dockerhub](https://hub.docker.com/)
3. Create an [organization](https://docs.docker.com/docker-hub/orgs/)
   * This is a temporary workaround, so pick anything for now

## Install the correct CLI binary

1. Download the distribution for [Linux (amd64)](https://s3.amazonaws.com/a8clirepository/linux-amd-64/a8) or 
[OSX](https://s3.amazonaws.com/a8clirepository/osx/a8)
2. Make sure the file is an executable:
	
	`sudo chmod +x /path/to/download/a8`

3. Move the file to `/usr/local/bin`
4. Test that the CLI works by typing `a8 --help`. If you get a help message then it works 💃

## Run the daemon

1. On the command line run 

	`docker run -d --rm --name daemon -p 3035:3035 autom8network/node:0.0.1 a8-node  --http-port 3035 --libp2p-port 4004 --bootstrappers /ip4/3.209.172.134/tcp/4002/ipfs/Qmeq45rCLjFt573aFKgLrcAmAMSmYy9WXTuetDsELM2r8m`

	This will automatically pull the latest daemon docker image, run it, and connect to the a8 network.
	
2. Test that the daemon can talk to the a8 network by running `a8 apps`. If you see a list of apps instead of an error, it's party time.

## Known Issues

Whenever your computer sleeps the daemon will need to re-sync with the network. To do this, simply run:

`docker restart daemon`

⛑We're actively working on making sure you won't have to do this, so please bear with us!

<br /><br />

# Quick Start

## Project structure

We recommend keeping your functions organzized in apps of related functionality. Here's an example:

```
/app1
 ↳/function1
 ↳/function2
/app2
 ↳/function1
 ↳/function2
```

We will be adding functionality that will enable you to deploy entire apps from the app root directory, so keeping this folder structure will set you up nicely for that.

For now, go to your favorite directory and create an app directory called `<your name>-functions` with one function directory called `helloWorld`. Then, `cd helloWorld`.

## Creating function scaffolding

The first thing you'll want to do is create a function scaffolding for your new function. This process initializes a directory with a hello world function in the specified runtime. In your function directory, run the following command

`a8 init --runtime node <app name>.<funciton name>`
  
⛑  <app name> and <function name> must only use lowercase letters and `_`. Further, they must be between 3 and 30 characters.
  
## Code your function

We initialized a function using the node runtime, so let's have a look at func.js

``` javascript
const fdk=require('@fnproject/fdk');
const a8=require('@autom8/js-a8-fdk')

fdk.handle(function(input){
  let name = 'World';
  if (input.name) {
    name = input.name;
  }
  return {'message': 'Hello ' + name}
})
```

Pretty straightforward. All you need to do is change the handle function to implement your desired functionality. For now, though, let's leave it as is and do a deploy.

## Deploying your function

To deploy your function, simply issue the following command:

`a8 deploy --repository <your docker org name>`

⤍ Tip: You created your docker org name as part of the [Installation prerequisites] section(#pre-requisites).<br/>

No errors? Then you just deployed your first function to the autom(8) network. Groovy! 🕺

## Invoking your function

Now that you've deployed your function, let's give it a test drive. One thing you'll need to know is your function's fully qualified name, which is simply `your-app-name.function-name`. Once you have that, simply issue the command:

`a8 invoke your-app-name.helloWorld`

This will return the following output:

```
{"message":"Hello World"}
```

How about inputs? Simple. Issue the following command:

`a8 invoke your-app-name.helloWorld '{"name":"Elmo"}'`

This will return the following output:

```
{"message":"Hello Elmo"}
```

Easy, right? Now it's off the the races!! 🏁🏁🏁🏁🏁🏁